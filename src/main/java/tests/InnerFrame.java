package tests;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PSM;

public class InnerFrame extends PSM {
	@BeforeTest
    public void setData() {
        testCaseName = "inner frame";
        testDescription = "inner frame switch";
        testNodes = "click in inner frame";
        category = "Smoke";
        authors = "Testleaf";
        browserName = "chrome";
    }
	@Test
	public void innerframe() {
		
		switchToInnerFrame(locateElement("xpath", "(//iframe)[2]"));
		click(locateElement("xpath", "//button[text()='Click Me']"));
	}
}
